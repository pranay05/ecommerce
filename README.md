
# README

* Ruby version: 2.4.1
* Rails version: rails, 5.1.6
* pg version: rails, 5.1.6
* psql (PostgreSQL) 10.5

How to run this project

1) clone project (git clone https://bitbucket.org/pranay05/ecommerce.git)

2) bundle install for fetching all gem libraries

3) rake db:create

4) rake db:migrate

5) rails s

6) To test the APIs download and export the test postman collection present at path '<project_path>/postman_test_collection/Ecommerce.postman_collection'



Summary
---------

This Project Follows RDD(Responsibility Driven Design) and this project has been broken down into 4 components.

1) Item Management - Item management contains set of APIs which helps in creation, reading, updation, deletion of item entity. Item entity
	will have admin owner who will belong to user class, distinguished by created_by.
2) Order Management - Order Management Component will have set of Apis to Manage order of specific user (by email).
	it will have Place order Apis to place order for item along with listing of all user
3) User management - This component has user creation Apis along with user address Creating, listing and updation

4) Cart management - This component has addition of item to cart, removal of item from cart and adding large quantity of item to cart.


Assumptions
-----------
1) We have not used any user authorization, we have created only user with the help of email and name without any login credentials.

2) not used session to store cart persitence, we have only used cart table .

3) no seed data as been pre populated

4) Assuming all items are in same currency for now. Taking it implicitly as INR.

5) Authentication and authorisation is handled separately and have not been included in this app. Can be achieved through:
Authentication: Devise, Sorcery, manually, etc
Authorisation: CanCAnCan, Pundit, manually, etc.

6) List item API lists items by price (low to high). There can be several other means as well.

7) The items present in carts make the respective items quantities unavailable for others. Later a logic can be implemented to hold the items in the cart only for some fixed threshold duration.



APIs Contracts
----------------

To test the APIs download and export the test postman collection present at path '<project_path>/postman_test_collection/Ecommerce.postman_collection'

1) Create User API

   HTTP method: POST
   HTTP headers: {'Content-Type': 'application/json'}
   Endpoint: {base_url}/users
   Request payload: { "user": {"name": "pranay", "email": "pranay@gmail.com" } }

2) Add User Address

	HTTP metod: POST
	HTTP headers: {'x-impersonate-email': <email>, 'Content-Type': 'application/json'}
	Endpoint: {base_url}/user/add_address
	Request payload: {"email": "pranay@gmail.com", "address_line": "addres line", "city": "delhi", "pincode": "226016"}

3) Update User Address

	HTTP method: POST
	HTTP headers: {'x-impersonate-email': <email>, 'Content-Type': 'application/json'}
	Endpoint: {base_url}/user/update_address
	Request payload: {"email": "pranay@gmail.com", "address_id": 5, "address_line": "addres line 1 updated", "city": "lucknow", "pincode": "226016"}

4) List User Address

	HTTP method: GET
	HTTP headers: {'x-impersonate-email': <email>, 'Content-Type': 'application/json'}
	Endpoint: {base_url}/list_address?email=pranay@gmail.com

5) Create Item

	HTTP method: POST
	HTTP headers: {'x-impersonate-email': <email>, 'Content-Type': 'application/json'}
	Endpoint: {base_url}/items
	request_payload:  {"item": {"title": "product 4", "description": "loren ipusm", "price": 1000, "quantity": 7,"img_url": "wewewe","created_by_id": 1 }}

6) Read item

	HTTP method: GET
	HTTP headers: {'x-impersonate-email': <email>, 'Content-Type': 'application/json'}
	Endpoint: {base_url}/items/:id

7) List Items:
    List item API lists items by price (low to high). There can be several other means as well.

	HTTP method: GET
	HTTP headers: {'x-impersonate-email': <email>, 'Content-Type': 'application/json'}
	Endpoint: {base_url}/items

8) Update item

	HTTP method: PATCH
	HTTP headers: {'x-impersonate-email': <email>, 'Content-Type': 'application/json'}
	Endpoint: {base_url}/items/:id

9) Delete item

	HTTP method: DELETE
	HTTP headers: {'x-impersonate-email': <email>, 'Content-Type': 'application/json'}
	Endpoint: {base_url}/items/:id

10) Upsert cart item:
    Cart is considered as a transient information. As soon as it converts to an order all items including are cart object are removed from persistence layer.
    This API upserts the items in the cart and changes (note it does not increments/decrements) the quantity of the given item to the one that is being sent in the API.
    It creates a cart object if not already exists (for a user).

	HTTP method: POST
	HTTP headers: {'x-impersonate-email': <email>, 'Content-Type': 'application/json'}
	Endpoint: {base_url}/carts/upsert_item
	request_payload: {"item_id": 4, "quantity": 2}

11) Get user Cart

	HTTP method: GET
	HTTP headers: {'x-impersonate-email': <email>, 'Content-Type': 'application/json'}
	Endpoint: {base_url}/carts/get_cart?email=pranay@gmail.com

12) remove item from Cart

	HTTP method: POST
    HTTP headers: {'x-impersonate-email': <email>, 'Content-Type': 'application/json'}
	Endpoint: {base_url}/carts/remove_item_from_cart?email=pranay@gmail.com
	request_payload: {"item_id": 4}

13) Place user order

	HTTP method: POST
	HTTP headers: {'x-impersonate-email': <email>, 'Content-Type': 'application/json'}
	Endpoint: {base_url}/order/create_order
	request_payload: {"email": "pranay@gmail.com", "address_id": 5}

14) Get all User Order

	HTTP method: GET
	HTTP headers: {'x-impersonate-email': <email>, 'Content-Type': 'application/json'}
	Endpoint: {base_url}/order/list_user_orders?email=pranay@gmail.com
