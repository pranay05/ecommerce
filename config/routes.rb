Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  post '/users' => 'users#create_user'

  resources :items, except: :edit

  post 'carts/upsert_item' => 'carts#upsert_item'
  get 'carts/get_cart' => 'carts#get_cart'
  post 'carts/remove_item_from_cart' => 'carts#remove_item_from_cart'

  post 'order/create_order' => 'orders#create_order'

  get 'order/list_user_orders' =>  'orders#list_user_orders'

  post 'user/add_address' => 'users#add_address'
  get 'user/list_address' => 'users#list_address'
  post 'user/update_address' => 'users#update_address'


end
