class AddUserIdIndexToAddresses < ActiveRecord::Migration[5.1]
  def change
    add_index :addresses, :user_id
  end
end
