class AddUserIdIndexToOrders < ActiveRecord::Migration[5.1]
  def change
    add_index :orders, :user_id
  end
end
