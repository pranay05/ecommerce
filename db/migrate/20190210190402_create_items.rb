class CreateItems < ActiveRecord::Migration[5.1]
  def change
    create_table :items do |t|
      t.string :title, null: false
      t.string :description
      t.decimal :price, null: false , default: 0
      t.integer :quantity, default: 0
      t.string :img_url
      t.boolean :is_deleted, default: false
      t.integer :created_by_id

      t.timestamps
    end
  end
end
