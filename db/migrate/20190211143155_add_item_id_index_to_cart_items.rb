class AddItemIdIndexToCartItems < ActiveRecord::Migration[5.1]
  def change
    add_index :cart_items, :item_id
    add_index :cart_items, :cart_id
  end
end
