class CreateOrders < ActiveRecord::Migration[5.1]
  def change
    create_table :orders do |t|
      t.integer :user_id
      t.decimal :total_price, default: 0
      t.string :status
      t.integer :address_id

      t.timestamps
    end
  end
end
