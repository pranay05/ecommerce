class CreateAddresses < ActiveRecord::Migration[5.1]
  def change
    create_table :addresses do |t|
      t.integer :user_id
      t.string :address_line, null: false
      t.string :city, null: false
      t.string :pincode, null: false

      t.timestamps
    end
  end
end
