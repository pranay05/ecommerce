class AddUserIdIndexToCarts < ActiveRecord::Migration[5.1]
  def change
    add_index :carts, :user_id
  end
end
