class CurrentCart

  def self.find_or_create_cart(user)
    @cart = Cart.find_by(user_id: user.id) || Cart.create(user_id: user.id, status: "created")
  end
end