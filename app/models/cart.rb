class Cart < ApplicationRecord
  has_many :cart_items, dependent: :destroy
  has_many :items, through: :cart_items
  belongs_to :user


  def upsert_item_in_cart(item, quantity)
    old_quantity_in_cart = 0
    current_item = cart_items.find_by(item_id: item.id)

    if current_item
      old_quantity_in_cart = current_item.quantity
      current_item.update_column(:quantity, quantity)
    else
      current_item = cart_items.create(item_id: item.id, quantity: quantity)
    end
    old_quantity_in_cart
  end

  def get_item(item_id)
    cart_items.where(item_id: item_id).first
  end

  def destroy_item_from_cart(item)
    cart_items.where(item_id: item.id).destroy_all
    true
  end

  def get_quantity_for_item(item)
    qty = 0
    cart_items.where(item_id: item.id).each do |cart_item|
      qty +=  cart_item.quantity
    end
    qty
  end

  def fetch_total_price
    sum = 0
    cart_items.each do |cart_item|
      sum += cart_item.item.price.to_i * cart_item.quantity
    end
    sum
  end


end
