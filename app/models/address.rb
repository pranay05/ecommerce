class Address < ApplicationRecord
  belongs_to :user

  validates :address_line, :city, :pincode,presence: true
  validates :address_line, length: { maximum: 1000, too_long: "%{count} characters is the maximum allowed" }
  validates :pincode, numericality: { only_integer: true }, length: { maximum: 7 }
end
