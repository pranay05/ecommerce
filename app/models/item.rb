class Item < ApplicationRecord
  belongs_to :created_by, foreign_key: 'created_by_id' , :class_name =>  'User'
  has_many :order_items
  has_many :orders, through: :order_items
  has_many :cart_items

  validates :title, :price, :quantity, presence: true
  validates :description, length: { maximum: 1000, too_long: "%{count} characters is the maximum allowed" }
  validates :title, length: { maximum: 140, too_long: "%{count} characters is the maximum allowed" }
  validates :price, numericality: true, length: { maximum: 7 }

  def persist_quantity_difference(qty_difference)
    self.increment!(:quantity, qty_difference)
    validate_if_valid_quantity
  end

  def self.get_item(id)
    item = find(id)
    raise ActiveRecord::RecordNotFound  if item.nil?
    return item
  end

  def validate_if_valid_quantity
    if self.quantity < 0
      raise Exception.new("Given quantity not available")
    end
  end
end
