class User < ApplicationRecord
  has_many :addresses, dependent: :destroy
  has_many :order_items
  has_many :orders
  has_one :cart

  validates :name, :email,presence: true
  validates :email, format: { with: URI::MailTo::EMAIL_REGEXP }
end
