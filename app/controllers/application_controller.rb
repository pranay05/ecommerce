class ApplicationController < ActionController::Base
  #protect_from_forgery with: :exception

  before_action :set_current_user, :except => [:create_user]

  def set_current_user
    # currently the app allows anyone to come and impersonate as any user by attaching the email.
    # This will go away with implementation of proper authetication.

    email = request.headers['x-impersonate-email']
    if email.blank?
      render json: {error: "unable to determine current user. Ensure header value for 'x-impersonate-email' is set"}, status: 401 and return
    end

    @current_user = User.find_by(email: email)
    render json: {error: "user with email #{email} not found"}, status: 401 and return if !@current_user
  end
end
