class CartsController < ApplicationController
  after_action :update_cart_price, only: [:upsert_item, :remove_item_from_cart]

  def upsert_item
    quantity = params[:quantity]
    raise Exception.new('Quantity needs to be positive') if (quantity <= 0)

    item = Item.get_item(params[:item_id])
    ActiveRecord::Base.transaction do
      cart = CurrentCart.find_or_create_cart(@current_user)
      old_quantity_in_cart = cart.upsert_item_in_cart(item, quantity)
      item.persist_quantity_difference(old_quantity_in_cart - quantity)
    end
    render json: {message: 'Upserted item in cart'}, status: 200

  rescue Exception => e
    Rails.logger.error(' ------- Message:' + e.message + ' ------- Backtrace:' + e.backtrace.join("\n"))

    error_msg = 'ooops! something went wrong'
    render json: { message: nil, error_msg:  error_msg, error: e.message }, status: 400
  end

  def remove_item_from_cart
    @cart = @current_user.cart

    @item = Item.find(params[:item_id])
    cart_item = @cart.get_item(params[:item_id])
    raise ActiveRecord::RecordNotFound  if @item.nil?

    ActiveRecord::Base.transaction do
      # todo remove cart items and add same quantity back to item object
      qty = @cart.get_quantity_for_item(@item)
      @item.persist_quantity_difference(qty)
      if @cart.destroy_item_from_cart(@item)
        render json: {message: 'Removed item from cart'}, status: 200
      end
    end

  rescue Exception => e
    error_msg = 'ooops! something went wrong'
    render json: {
        message: nil,
        error_msg:  error_msg,
        error: e.message
    }, status: 400
  end

  def get_cart
    user_cart = @current_user.cart.cart_items
    raise ActiveRecord::RecordNotFound  if user_cart.nil?
    cart = {
        cart_id: @current_user.cart.id
    }

    build_cart_items_response(cart, user_cart)

    render json: cart

  rescue Exception => e
    error_msg = 'ooops! something went wrong'
    render json: {
        message: nil,
        error_msg:  error_msg,
        error: e.message
    }, status: 400
  end


  private
  def build_cart_items_response(cart, user_cart)
    cart[:item_list] = user_cart.as_json
    cart_total_price = 0
    cart[:item_list].each do |cart_item|
      # todo: problem it will exectute N+1 queries
      item = Item.find(cart_item['item_id'])
      cart_item[:item_info] = item.as_json
      cart_total_price += item.price * cart_item['quantity']

      cart_item.delete('id')
      cart_item.delete('cart_id')
      cart_item.delete('item_id')
    end
    cart[:cart_total] = cart_total_price
  end

  def update_cart_price
    @cart =  CurrentCart.find_or_create_cart(@current_user)
    cart_price = @cart.fetch_total_price
    @cart.update_column(:total_price,cart_price)
  end



  def cart_params
    params.require(:cart).permit(:id, :user_id, :total_price, :status)
  end
end
