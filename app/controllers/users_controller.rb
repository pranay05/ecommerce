class UsersController < ApplicationController

  def create_user
    user = User.new(user_params)

    if user.save
      render json: {message: 'User Has Been Created', user: user.as_json}, status: :created
    end
  rescue Exception => e
    error_msg = 'ooops! something went wrong'
    render json: {
        message: nil,
        error_msg:  error_msg,
        error: e.message
    }, status: 400
  end

  def add_address
    user =  @current_user
    raise Exception.new("user with #{params[:email]} not found")  if user.nil?
   @address =  Address.new(user_id: user.id, address_line: params[:address_line], city: params[:city], pincode: params[:pincode])
   if @address.save
     render json: {message: 'Address added', address: @address.as_json}, status: :created
   else
     render json: @address.errors, status: :unprocessable_entity
   end

  rescue Exception => e
    error_msg = 'ooops! something went wrong'
    render json: {
        message: nil,
        error_msg:  error_msg,
        error: e.message
    }, status: 400
  end

  def list_address
    user =  @current_user
    raise Exception.new("user with #{params[:email]} not found")  if user.nil?
    addresses = user.addresses
    render json: addresses.as_json, status: 200

  rescue Exception => e
    error_msg = 'ooops! something went wrong'
    render json: {
        message: nil,
        error_msg:  error_msg,
        error: e.message
    }, status: 400
  end

 def update_address
   user =  @current_user
   raise Exception.new("user with #{params[:email]} not found")  if user.nil?
   @address = Address.where(id: params[:address_id], user_id: user.id).first
   raise Exception.new("Adress with email #{@current_user.email} and address id = #{ params[:address_id]} not found")  if  @address.blank?

  if  @address.update_attributes(address_line: params[:address_line], city: params[:city], pincode: params[:pincode])
    render json: {message: 'User Address has Been updated'}, status: 200
  else
    render json: @address.errors, status: :unprocessable_entity
  end

 rescue Exception => e
   error_msg = 'ooops! something went wrong'
   render json: {
       message: nil,
       error_msg:  error_msg,
       error: e.message
   }, status: 400

 end

  private
  def user_params
    params.require(:user).permit(:id, :name, :email)
  end
end
