class ItemsController < ApplicationController
  before_action :set_item, only: [:show, :update, :destroy]

  DEFAULT_PAGE_SIZE = 20

  def index
    page_size = params[:page_size] || DEFAULT_PAGE_SIZE
    @items = Item.all.where(is_deleted: false).order('price desc').paginate(:page => params[:page], :per_page => page_size)
    list_items = []
    items = {
        "page_no": @items.current_page,
        "page_size": @items.per_page,
        "total_entries": @items.total_entries,
        "has_more": @items.next_page.present?,
    }
    @items.each do |item|
      list_item = {
          "id": item.id,
          "title": item.title,
          "description": item.description,
          "price": item.price,
          "quantity": item.quantity,
          "img_url": item.img_url,
          "quantity": item.quantity
      }
      list_items << list_item
    end
    items["list_items"] = list_items
    render json: items
  end

  def create
    new_item_params = item_params
    new_item_params[:created_by_id] = @current_user.id

    @item = Item.new(new_item_params)
    if @item.save
      render json: {message: 'Item Has Been Created', item: @item.as_json }, status: :created
    else
      render json:  @item.errors, status: :unprocessable_entity
    end

  rescue Exception => e
    error_msg = 'ooops! something went wrong'
    render json: {
        message: nil,
        error_msg:  error_msg,
        error: e.message
    }, status: 400
  end

  def show
    render json: @item.as_json

  rescue Exception => e
    error_msg = 'ooops! something went wrong'
    render json: {
        message: nil,
        error_msg:  error_msg,
        error: e.message
    }, status: 400
  end

  def update
    if @item.update(item_params)
      render json: {message: 'Item Has Been Updated', item: @item.as_json}, status: 200
    else
      render json:  @item.errors, status: :unprocessable_entity
    end

  rescue Exception => e
    error_msg = 'ooops! something went wrong'
    render json: {
        message: nil,
        error_msg:  error_msg,
        error: e.message
    }, status: 400
  end

  def destroy
   if @item.update_column(:is_deleted, true)
    render json: {message: 'Item Has Been Deleted'}, status: 200
   else
     render json:  @item.errors, status: :unprocessable_entity
   end

  rescue Exception => e
    error_msg = 'ooops! something went wrong'
    render json: {
        message: nil,
        error_msg:  error_msg,
        error: e.message
    }, status: 400
  end

  private

  def set_item
    @item = Item.find(params[:id])
  end

  def item_params
    params.require(:item).permit(:id, :title, :description, :price, :quantity, :img_url, :is_deleted)
  end
end
