class OrdersController < ApplicationController

  def create_order
    user = @current_user

    cart_items = get_items_for_order(user)

    ActiveRecord::Base.transaction do
      # todo this is not correct. Calculate total price realtime as prices may have changed
      order = Order.create(user_id: user.id, address_id: params[:address_id], status: 'PLACED', total_price: user.cart.total_price)
      cart_total_price = 0

      cart_items.each do |cart_item|
        @item = Item.find(cart_item.item_id)
        tp = cart_item.quantity.to_i *  @item.price.to_i
        cart_total_price += tp

        order_item = OrderItem.new(order_id: order.id, item_id: cart_item.item_id, quantity: cart_item.quantity, total_price: tp )
        order_item.save
      end
      order.update_column(:total_price, cart_total_price)

      empty_user_cart(user.cart)
    end

    render json: {message: 'User Order has Been placed'}, status: :created
  rescue Exception => e
    error_msg = 'ooops! something went wrong'
    render json: {
        message: nil,
        error_msg:  error_msg,
        error: e.message
    }, status: 400
  end

  def list_user_orders
    user = @current_user

    orders = []
    user.orders.each do |order|
      order_item_list = []

      order.order_items.each do |order_item|
        order_item_list << order_item.as_json
      end

      orders << {
          order_id: order.id,
          total_price: order.total_price,
          order_items: order_item_list
      }
    end

    render json: {user_orders: orders}
  end

  private

  def get_items_for_order(user)
    cart_items = user.cart&.cart_items
    raise Exception.new("Cart is empty") if cart_items.blank?
    cart_items
  end

  def  empty_user_cart(cart)
    cart.update_attributes(total_price: 0, status: nil)
    cart.cart_items.destroy_all
  end


end
